'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('../server.js');

describe('Get user - GET /api/user/{id}', () => {
    let server;
    let token;
    let user;

    beforeEach(async () => {
        server = await init();
        const res = await server.inject({
            method: 'post',
            url: '/api/login',
            headers: {
                "Content-Type:": "application/json"
            },
            payload: {"email":"admin@test.com","password":"admin123"}
        });
        token = res.result.token;

        const userRes = await server.inject({
            method: 'get',
            url: '/api/user?page=1&perPage=10&sortField=createdAt&sortOrder=descend',
            headers: {
                "Content-Type:": "application/json",
                Authorization: `Bearer ${token}`
            }
        });
        user = userRes.result.list[0];
    });

    afterEach(async () => {
        await server.stop();
    });

    it('responds with 200 when get a user', async () => {
        const res = await server.inject({
            method: 'get',
            url: `/api/user/${user.id}`,
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        expect(res.statusCode).to.equal(200);
        expect(res.result).to.object();

        const userData = res.result;
        expect(userData.id).to.equal(user.id);
    });

    it('responds with 404 when the user is not found', async () => {
        const res = await server.inject({
            method: 'get',
            url: `/api/user/fsdf3243`,
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        expect(res.statusCode).to.equal(404);
    });

    it('responds with 401 when when token is not valid', async () => {
        const res = await server.inject({
            method: 'get',
            url: `/api/user/${user.id}`,
            headers: {
                Authorization: `Bearer sdfsd434`
            }
        });
        expect(res.statusCode).to.equal(401);
    });
});
