'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('../server.js');
const Util = require("../util/utilFunctions");

describe('Update user - PUT /api/user/{id}', () => {
    let server;
    let token;

    beforeEach(async () => {
        server = await init();
        const res = await server.inject({
            method: 'post',
            url: '/api/login',
            headers: {
                "Content-Type:": "application/json"
            },
            payload: {"email":"admin@test.com","password":"admin123"}
        });
        token = res.result.token;
    });

    afterEach(async () => {
        await server.stop();
    });

    it('responds with 200 when updates a user', async () => {
        const id = Util.getRandomString(10);
        const name = `Pepe-${id}`;
        const lastName = `Perez-${id}`;
        const email = `Pepe-${id}@gmail.com`;
        const res = await server.inject({
            method: 'put',
            url: '/api/user/f6adae70-b59a-11ea-bf4b-67edba3d0fea',
            headers: {
                Authorization: `Bearer ${token}`
            },
            payload: {
                "firstName":name,
                "lastName":lastName,
                "email":email,
                "role":1,
                "id":"b74baa20-9236-11ea-9295-91f4ea444469"
            }
        });
        expect(res.statusCode).to.equal(200);
        expect(res.result).to.object();

        const user = res.result;
        expect(user.firstName).to.equal(name);
        expect(user.lastName).to.equal(lastName);
        expect(user.role).to.equal(1);
    });

    it('responds with 401 when token is invalid', async () => {
        const id = Util.getRandomString(10);
        const name = `Pepe-${id}`;
        const lastName = `Perez-${id}`;
        const email = `Pepe-${id}@gmail.com`;
        const res = await server.inject({
            method: 'put',
            url: '/api/user/b74baa20-9236-11ea-9295-91f4ea444469',
            headers: {
                Authorization: `Bearer `
            },
            payload: {
                "firstName":name,
                "lastName":lastName,
                "email":email,
                "role":1,
                "id":"b74baa20-9236-11ea-9295-91f4ea444469"
            }
        });
        expect(res.statusCode).to.equal(401);
    });

    it('responds with 400 when email is invalid', async () => {
        const id = Util.getRandomString(10);
        const name = `Pepe-${id}`;
        const lastName = `Perez-${id}`;
        const email = `@gmail.com`;
        const res = await server.inject({
            method: 'put',
            url: '/api/user/b74baa20-9236-11ea-9295-91f4ea444469',
            headers: {
                Authorization: `Bearer ${token}`
            },
            payload: {
                "firstName":name,
                "lastName":lastName,
                "email":email,
                "role":1,
                "id":"b74baa20-9236-11ea-9295-91f4ea444469"
            }
        });
        expect(res.statusCode).to.equal(400);
    });

    it('responds with 400 when role is invalid', async () => {
        const id = Util.getRandomString(10);
        const name = `Pepe-${id}`;
        const lastName = `Perez-${id}`;
        const email = `Pepe-${id}@gmail.com`;
        const res = await server.inject({
            method: 'put',
            url: '/api/user/b74baa20-9236-11ea-9295-91f4ea444469',
            headers: {
                Authorization: `Bearer ${token}`
            },
            payload: {
                "firstName":name,
                "lastName":lastName,
                "email":email,
                "role":0,
                "id":"b74baa20-9236-11ea-9295-91f4ea444469"
            }
        });
        expect(res.statusCode).to.equal(400);
    });

    it('responds with 404 when updates a user', async () => {
        const id = Util.getRandomString(10);
        const name = `Pepe-${id}`;
        const lastName = `Perez-${id}`;
        const email = `Pepe-${id}@gmail.com`;
        const res = await server.inject({
            method: 'put',
            url: '/api/user/b74baa20-9236-11ea-9295-91f4ea44446',
            headers: {
                Authorization: `Bearer ${token}`
            },
            payload: {
                "firstName":name,
                "lastName":lastName,
                "email":email,
                "role":1,
                "id":"b74baa20-9236-11ea-9295-91f4ea44446"
            }
        });
        expect(res.statusCode).to.equal(404);
    });
});
