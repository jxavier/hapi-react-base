import React from 'react';
import './App.pcss';
import {
    BrowserRouter as Router,
    Switch,
    Route, Link
} from "react-router-dom";
import {Home} from "./pages/Home"
import Login from "./pages/Login"
import 'antd/dist/antd.css';
import AppLayout from "./pages/appLayout/AppLayout";
import Users from "./pages/users/Users";
import {Provider} from "mobx-react";
import {EntityManager} from "./services/EntityManager";
import I18nText from "./components/i18nText/I18nText";

function App() {
    const entityManager = new EntityManager();
    return (
        <Provider store={entityManager}>
            <Router>
                <Switch>
                    <Route path="/login">
                        <Login/>
                    </Route>
                    <Route path="/users">
                        <AppLayout pageIndex='2'
                                   breadcrumbs={[
                                       {key:"home", element: <Link to="/"><I18nText textKey="Home"/></Link>},
                                       {key: "users", element: <Link to="/users"><I18nText textKey="Users"/></Link>}
                                   ]}
                        >
                            <Users/>
                        </AppLayout>
                    </Route>
                    <Route path="/">
                        <AppLayout pageIndex='1'
                                   breadcrumbs={[
                                       {key:"home", element: <Link to="/"><I18nText textKey="Home"/></Link>}
                                   ]}
                        >
                            <Home/>
                        </AppLayout>
                    </Route>
                </Switch>
            </Router>
        </Provider>
    );
}

export default App;
