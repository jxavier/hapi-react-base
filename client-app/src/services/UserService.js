import {EntityService} from "./EntityService";
import {User} from "../models/User";
import {observable, decorate} from "mobx";

export class UserService extends EntityService{
    user = null;
    constructor(updateToken){
        super(updateToken);
        let userString = localStorage.getItem("appAuth");
        if(userString){
            this.user = JSON.parse(userString);
        }
    }

    authenticate(email, password){
        const self = this;
        return new Promise(function (resolve, reject) {
            self.request({
                url: '/api/login',
                method: "POST",
                requireAuth: false,
                body: {email: email, password: password}
            }).then(data => {
                self.user = data;
                localStorage.setItem("appAuth", JSON.stringify(data));
                self.updateToken(data);
                resolve(data);
            }).catch(error => {
                reject(error);
            });
        });
    }

    logout(){
        localStorage.setItem("appAuth", "");
        this.token = "";
        this.user = null;
    }

    list(page, perPage, sortField, sortOrder){
        console.log("list function");
        return this.request({
            url: `/api/user?page=${page}&perPage=${perPage}&sortField=${sortField}&sortOrder=${sortOrder}`,
            method: "GET",
            requireAuth: true,
            transformData: function (data) {
                data.list = data.list.map(user => new User(user));
                return data;
            }
        });
    }

    create(user){
        return this.request({
            url: '/api/user',
            method: "POST",
            requireAuth: true,
            body: user
        });
    }

    update(user){
        return this.request({
            url: '/api/user/'+user.id,
            method: "PUT",
            requireAuth: true,
            body: user
        });
    }

    delete(user){
        return this.request({
            url: '/api/user/'+user.id,
            method: "DELETE",
            requireAuth: true
        });
    }
}
decorate(UserService, {
    user: observable
});
