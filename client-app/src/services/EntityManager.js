import {UserService} from "./UserService";
import {observable, decorate} from "mobx";
import locales from "../languages/locales";
import en from "../languages/en";
import es from "../languages/es";

export class EntityManager{
    locale = "es";
    locales = locales;
    constructor(){
        const self = this;
        function updateUser(user) {
            self.user = user;
            self.setUser(user);
        }
        this.userService = new UserService(updateUser);
        let userString = localStorage.getItem("appAuth");
        if(userString){
            this.user = JSON.parse(userString);
            this.setUser(this.user);
        }
    }

    setHistory(history){
        this.history = history;
        this.userService.setHistory(history);
    }

    setUser(token){
        this.userService.setUser(token);
    }

    logout(){
        localStorage.setItem("appAuth", "");
        this.user = {};
        this.userService.logout();
    }

    setNotificationComponent(notifier){
        //this.notifier = new Notifier(notifier);
        //this.userService.setNotification(this.notifier);
    }
    setLocale(locale){
        this.locale = locale;
    }
    localeFile(){
        switch (this.locale) {
            case "en": return en;
            case "es": return es;
            default: return en;
        }
    }
    getMessage(textKey){
        return this.localeFile()[textKey];
    }
}
decorate(EntityManager, {
    locale: observable
});
