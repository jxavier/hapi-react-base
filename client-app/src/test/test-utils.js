import React from 'react'
import { render } from '@testing-library/react'
import {EntityManager} from "../services/EntityManager";
import {Provider} from "mobx-react";

const entityManager = new EntityManager();
const AllTheProviders = ({ children }) => {
    return (
        <Provider store={entityManager}>
            {children}
        </Provider>
    )
};

const customRender = (ui, options) =>
    render(ui, { wrapper: AllTheProviders, ...options });

// re-export everything
export * from '@testing-library/react'

// override render method
export { customRender as render }
