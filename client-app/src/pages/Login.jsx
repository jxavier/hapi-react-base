import React, {Component} from "react";
import { Row, Col, Card, Form, Icon, Input, Button, Checkbox, Select } from 'antd';
import {observer, inject} from "mobx-react";
import {Redirect} from "react-router-dom";
import I18nText from "../components/i18nText/I18nText";

const { Option } = Select;

class Login extends Component{

    constructor(props, context) {
        super(props);
        this.state = {
            loading: false
        }
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.setState({
                    loading: true
                });
                this.props.store.userService.authenticate(values.email, values.password).catch(error=> {
                    this.setState({
                        loading: false
                    });
                });
            }
        });
    };
    handleChange(value) {
        this.props.store.setLocale(value)
    }

    render() {
        if(!this.props.store.userService.user){
            const { getFieldDecorator } = this.props.form;
            return (
                <div>
                    <Row type="flex" justify="end">
                        <Col md={6} sm={22}>
                            <Select defaultValue="es" style={{ width: 60, margin: 10, float: "right" }} onChange={this.handleChange.bind(this)}>
                                {this.props.store.locales.map(locale=> <Option key={"locale."+locale.locale} value={locale.locale}>{locale.flag}</Option>)}
                            </Select>
                        </Col>
                    </Row>
                    <Row type="flex" justify="center">
                        <Col md={6} sm={22}>
                            <Card style={{ marginTop: '20%' }}>
                                <Form onSubmit={this.handleSubmit} className="login-form">
                                    <Form.Item>
                                        {getFieldDecorator('email', {
                                            rules: [{ required: true, message: this.props.store.getMessage("Please input your email") }],
                                        })(
                                            <Input
                                                prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                                placeholder={this.props.store.getMessage("Email")}
                                            />,
                                        )}
                                    </Form.Item>
                                    <Form.Item>
                                        {getFieldDecorator('password', {
                                            rules: [{ required: true, message: this.props.store.getMessage("Please input your password") }],
                                        })(
                                            <Input
                                                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                                type="password"
                                                placeholder={this.props.store.getMessage("Password")}
                                            />,
                                        )}
                                    </Form.Item>
                                    <Form.Item>
                                        {getFieldDecorator('remember', {
                                            valuePropName: 'checked',
                                            initialValue: true,
                                        })(<Checkbox><I18nText textKey="Remember me"/></Checkbox>)}
                                        <a className="login-form-forgot" href="" style={{float: 'right'}}>
                                            <I18nText textKey="Forgot password"/>
                                        </a>
                                        <Button loading={this.state.loading} type="primary" htmlType="submit" className="login-form-button" style={{width: '100%'}}>
                                            <I18nText textKey="Log in"/>
                                        </Button>
                                        <I18nText textKey="Or"/> <a href=""><I18nText textKey="register now"/></a>
                                    </Form.Item>
                                </Form>
                            </Card>
                        </Col>
                    </Row>
                </div>
            );
        }
        else{
            return <Redirect to="/"/>
        }
    }
}

Login = inject("store")(observer(Login));

const LoginPage = Form.create({ name: 'normal_login' })(Login);
export default LoginPage;
