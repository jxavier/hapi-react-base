import React, {Component} from "react";
import "./users.pcss";
import { Table, Button, Divider, Popconfirm } from 'antd';
import {observer, inject} from "mobx-react";
import UserForm from "../../components/userForm/UserForm";
import I18nText from "../../components/i18nText/I18nText";

class Users extends Component{

    constructor(props, context) {
        super(props, context);
        this.state = {
            data: {
                list: []
            },
            pagination: {},
            loading: false,
            editing: false,
            selectedUser: {}
        };
    }

    componentDidMount() {
        this.fetch();
    }

    fetch(params = {}){
        console.log('params:', this.props.store.userService.list);
        this.setState({ loading: true });
        this.props.store.userService.list(params.page || 1,
            params.results || 10,
            params.sortField || "",
            params.sortOrder || ""
        ).then(users => {
            console.log("users", users);
            const pagination = { ...this.state.pagination };
            pagination.total = users.count;
            this.setState({
                loading: false,
                data: users,
                pagination: pagination
            });
        }).catch(error => {
            this.setState({
                loading: false
            });
        });
    }
    handleTableChange = (pagination, filters, sorter) => {
        const pager = { ...this.state.pagination };
        pager.current = pagination.current;
        this.setState({
            pagination: pager,
        });
        this.fetch({
            results: pagination.pageSize,
            page: pagination.current,
            sortField: sorter.field,
            sortOrder: sorter.order,
            ...filters,
        });
    };
    showForm(){
        this.setState({
            editing: true,
            selectedUser: {}
        });
    }
    showList(){
        this.setState({
            editing: false
        });
    }
    onUserSaved(user){
        this.setState({
            editing: false
        });
        this.fetch();
    }
    deleteUser(user, event){
        this.setState({
            loading: true,
        });
        this.props.store.userService.delete(user).then(resp => {
            this.setState({
                loading: false,
            });
            this.fetch();
        }).catch(error => {
            console.error("error", error);
            this.setState({
                loading: false,
            });
        });
    }
    editUser(user){
        this.setState({
            editing: true,
            selectedUser: user
        });
    }

    render() {
        const columns = [
            {
                title: this.props.store.getMessage("Name"),
                dataIndex: 'firstName',
                key: 'name',
                sorter: true,
            },
            {
                title: this.props.store.getMessage("Last name"),
                dataIndex: 'lastName',
                key: 'lastName',
                sorter: true,
            },
            {
                title: this.props.store.getMessage("Email"),
                dataIndex: 'email',
                key: 'email',
                sorter: true,
            },
            {
                title: this.props.store.getMessage("Actions"),
                key: "actions",
                render: (text, record) => {
                    if(record.id !== this.props.store.userService.user.id){
                        return (
                            <span>
                                <a href="#" onClick={event => this.editUser(record)}><I18nText textKey="Update"/></a>
                                <Divider type="vertical" />
                                <Popconfirm
                                    title={this.props.store.getMessage("Are you sure delete this user?")}
                                    onConfirm={this.deleteUser.bind(this, record)}
                                    okText={this.props.store.getMessage("Yes")}
                                    cancelText={this.props.store.getMessage("No")}
                                >
                                    <a className="red-color" href="#"><I18nText textKey="Delete"/></a>
                                </Popconfirm>
                            </span>
                        );
                    }
                    else{
                        return <span>
                                    <a href="#" onClick={event => this.editUser(record)}><I18nText textKey="Update"/></a>
                                </span>;
                    }
                },
            }
        ];
        if(this.state.editing){
            return <div className="users-page">
                <Button onClick={this.showList.bind(this)} type="primary" style={{ marginBottom: 16 }}>
                    {this.props.store.getMessage("List")}
                </Button>
                <UserForm onUserSaved={this.onUserSaved.bind(this)} user={this.state.selectedUser}/>
            </div>
        }
        return (
            <div className="users-page">
                <Button onClick={this.showForm.bind(this)} type="primary" style={{ marginBottom: 16 }}>
                    {this.props.store.getMessage("Add a user")}
                </Button>
                <Table dataSource={this.state.data.list}
                       columns={columns}
                       pagination={this.state.pagination}
                       loading={this.state.loading}
                       rowKey={record => record.id}
                       onChange={this.handleTableChange}
                />
            </div>
        );
    }
}

Users = inject("store")(observer(Users));

export default Users;
