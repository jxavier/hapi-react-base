import React from 'react'
import { render, fireEvent, waitFor, screen } from '../../test/test-utils'
import '@testing-library/jest-dom/extend-expect'
import Users from "./Users";
import {UserService} from "../../services/UserService";
import {EntityManager} from "../../services/EntityManager";

test('loads user table', async () => {
    const users = {
        "list": [{
            "id": "d4d6cf20-b59a-11ea-9d1d-c53d3d53aec1",
            "firstName": "Admin",
            "lastName": "Super",
            "email": "admin@test.com",
            "role": 1,
            "photo": null,
            "createdAt": "2020-06-23T21:45:18.000Z",
            "updatedAt": "2020-06-23T21:45:18.000Z"
        }, {
            "id": "f6adae70-b59a-11ea-bf4b-67edba3d0fea",
            "firstName": "Pepe-9ws1CoNbSs",
            "lastName": "Perez-9ws1CoNbSs",
            "email": "Pepe-9ws1CoNbSs@gmail.com",
            "role": 1,
            "photo": null,
            "createdAt": "2020-06-23T21:46:15.000Z",
            "updatedAt": "2020-06-23T22:02:02.000Z"
        }], "count": 2
    };
    const entityManager = new EntityManager();
    entityManager.userService.user = {
        "id": "d4d6cf20-b59a-11ea-9d1d-c53d3d53aec1",
        "firstName": "Admin",
        "lastName": "Super",
        "email": "admin@test.com",
        "role": 1,
        "photo": null,
        "createdAt": "2020-06-23T21:45:18.000Z",
        "updatedAt": "2020-06-23T21:45:18.000Z",
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImQ0ZDZjZjIwLWI1OWEtMTFlYS05ZDFkLWM1M2QzZDUzYWVjMSIsImlhdCI6MTYwMTA2NzUxMX0.xzA8sQzdqaGbpIW2poS1sI98hg9oiCt6kCZk9fHo5Jk"
    };
    entityManager.userService.list = jest.fn(() => Promise.resolve(users));
    const { getByText } = render(<Users store={entityManager}/>);

    setTimeout(()=> {
        const linkElement = getByText("Agregar un usuario");
        const dataElement = getByText("Pepe-9ws1CoNbSs");
        const emailElement = getByText("admin@test.com");
        expect(linkElement).toBeInTheDocument();
        expect(dataElement).toBeInTheDocument();
        expect(emailElement).toBeInTheDocument();
    }, 100);
});
