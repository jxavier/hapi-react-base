const db = require("../models");
const envConfig = require("../config/config");
const env = process.env.NODE_ENV || 'development';
const bcrypt = require('bcrypt');
const jwt   = require('jsonwebtoken');
const userTemplate = require("../templates/userTemplate");
const Unauthorized = require("../exceptions/Unauthorized");
const uuid = require("uuid");
const _ = require("lodash");
const NotFound = require("../exceptions/NotFound");

module.exports = {
    login: async function (email, password) {
        let user = await db.User.findOne({
            where: {
                email: email
            }
        });
        let validUser = user && (await bcrypt.compareSync(password,user.password));

        if (validUser) {
            return {
                user: userTemplate.parseEntity(user),
                token: jwt.sign({ id: user.id }, envConfig[env].secret)
            };
        } else {
            throw new Unauthorized();
        }
    },
    list: async function (currentUser, page = 1, perPage = 10, sortField = "id", sortOrder = "ascend"){
        const order = sortOrder === "descend"?"DESC":"ASC";
        let field = "";
        if(_.isEmpty(sortField) || _.isEmpty(sortOrder)){
            field = "createdAt";
        }
        else{
            field = _.isEmpty(sortField)?"id":sortField;
        }
        if (currentUser) {
            let offset = (page - 1) * perPage;
            switch (currentUser.role) {
                case 1:
                    let userList = await db.User.findAndCountAll({
                        offset: offset,
                        limit: Number(perPage),
                        order: [[field, order],]
                    });
                    return {
                        list: userTemplate.parseList(userList.rows),
                        count: userList.count
                    };
                default: return {
                    list: [],
                    count: 0
                }
            }
        }
        else{
            return {
                list: [],
                count: 0
            }
        }
    },
    get: async function (id, currentUser){
        let user = null;
        if(currentUser){
            switch (currentUser.role) {
                case 1:
                    user = await db.User.findOne({
                        where: {
                            id: id
                        }
                    });
                    break;
                default: return null;
            }
        }
        else{
            user = await db.User.findOne({
                where: {
                    id: id
                }
            });
        }

        return userTemplate.parseEntity(user);
    },
    create: async function(userData){
        let user = await db.User.create({
            id: uuid.v1(),
            firstName: userData.firstName,
            lastName: userData.lastName,
            email: userData.email,
            role: userData.role,
            password: bcrypt.hashSync(userData.password, 12),
            createdAt: new Date(),
            updatedAt: new Date()
        });
        return userTemplate.parseEntity(user);
    },
    update: async function(id, userData){
        delete userData.id;
        if(!_.isEmpty(id)){
            const resp = await db.User.update(userData, {
                where: {
                    id: id
                }
            });
            if(resp < 1){
                throw new NotFound();
            }
            const user = await db.User.findOne({
                where: {
                    id: id
                }
            });

            return userTemplate.parseEntity(user);
        }
    },
    remove: async function(id){
        if(!_.isEmpty(id)){
            let resp = await db.User.destroy({
                where: {
                    id: id
                }
            });
            if(resp < 1){
                throw new NotFound();
            }
        }
    }
};
