const userService = require("../services/userService");
const errorResponse = require("../responses/errorResponse");
const emptyResponse = require("../responses/emptyResponse");
const Unauthorized = require("../exceptions/Unauthorized");
const NotFound = require("../exceptions/NotFound");
const BadRequest = require("../exceptions/BadRequest");
const createdResponse = require("../responses/createdResponse");

module.exports = {
    login: async function (request, h) {
        try {
            let { email, password } = request.payload;
            let loginResp = await userService.login(email, password);

            if (loginResp.token) {
                loginResp.user.token = loginResp.token;
                return loginResp.user;
            } else {
                const response = h.response({text: "Not authenticated"});
                response.statusCode = 401;
                return response;
            }
        }
        catch (e) {
            return errorResponse(e, h);
        }
    },
    list: async function (request, h){
        try{
            let user = await userService.get(request.auth.credentials.id);
            if(user){
                return await userService.list(user,
                    request.query.page,
                    request.query.perPage,
                    request.query.sortField,
                    request.query.sortOrder
                );
            }
            return errorResponse(new Unauthorized(), h);
        }
        catch (e){
            return errorResponse(e, h);
        }
    },
    get: async function (request, h){
        try{
            let user = await userService.get(request.auth.credentials.id);
            if(user){
                let searchUser = await userService.get(request.params.id, user);
                if(searchUser){
                    return searchUser;
                }
                return errorResponse(new NotFound(), h);
            }
            return errorResponse(new Unauthorized(), h);
        }
        catch (e){
            return errorResponse(e, h);
        }
    },
    create: async function(request, h){
        try{
            if(request.payload.password !== request.payload.confirmPassword){
                return errorResponse(new BadRequest("Password and confirmation do not match."), h);
            }
            const user = await userService.create(request.payload);
            return createdResponse(user, h);
        }
        catch(e){
            return errorResponse(e, h);
        }
    },
    update: async function(request, h){
        try{
            return await userService.update(request.params.id, request.payload);
        }
        catch(e){
            return errorResponse(e, h);
        }
    },
    remove: async function(request, h){
        try{
            let user = await userService.get(request.auth.credentials.id);
            if(user.id !== request.params.id){
                await userService.remove(request.params.id, user);
                return emptyResponse(h);
            }
            else{
                return new errorResponse(new BadRequest("The current user cannot delete itself."), h);
            }
        }
        catch(e){
            return errorResponse(e, h);
        }
    }
};
