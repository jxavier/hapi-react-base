const AppException = require("../exceptions/AppException");

module.exports = function (exception, h) {
    if(exception instanceof AppException){
        const response = h.response({text: exception.message});
        response.statusCode = exception.code;
        return response;
    }
    const response = h.response({text: "Oops, something went wrong."});
    response.statusCode = 500;
    return response;
};
