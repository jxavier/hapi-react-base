module.exports = function (entity, h) {
    const response = h.response(entity);
    response.statusCode = 201;
    return response;
};
