const AppException = require("./AppException");
const _ = require("lodash");

module.exports = class NotFound extends AppException{
    constructor(message) {
        super(message, 400);
        if(_.isEmpty(message)){
            this.message = "Bad request";
        }
    }
};
